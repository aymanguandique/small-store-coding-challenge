<?php
/**
 * The template for displaying full width pages.
 *
 * Template Name: About Page
 *
 * @package storefront
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div style="width:30%;padding:0 10px 0 0;float:left;">
				<img class="center-image" src="/wp-content/uploads/2019/03/profile-pic.png"/>
			</div>
 
			<div style="width:70%;padding:0 10px 0 0;float:right;">
				<?php
				while ( have_posts() ) :
					the_post();

					do_action( 'storefront_page_before' );

					get_template_part( 'content', 'page' );

					/**
				 	* Functions hooked in to storefront_page_after action
				 	*
				 	* @hooked storefront_display_comments - 10
				 	*/
					do_action( 'storefront_page_after' );

				endwhile; // End of the loop.
				?>
			</div>

			<div>
				<h1 class="center">SHOP</h1>
				<?php
					echo do_shortcode('[products ids="70, 75, 80, 110, 112"]');
				?>
			</div>
			<div class="back-button">
				<p><a href="/">Go back to home</a></p>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
	
<?php
get_footer();
