## Install & Configure Docker

1. Install docker from this website: https://www.docker.com/products/docker-desktop
2. Make sure Virtualization is enabled in BIOS.
3. If docker is having problems at launching follow these steps:
    SOLUTION A (If Hyper-V is totally disabled or not installed)
    1.	Open PowerShell as administrator and
    2.	Enable Hyper-V with
    dism.exe /Online /Enable-Feature:Microsoft-Hyper-V /All
    SOLUTION B (If Hyper-V feature is already enabled but doesn't work)
    Enable Hypervisor with
    bcdedit /set hypervisorlaunchtype auto
    Now restart the system and try again.
4. Make sure Docker is running on your computer

## Clone repository to your computer

1. Clone the repository on this link: https://bitbucket.org/aymanguandique/small-store-coding-challenge/src/master/ to any folder on your machine.

## Create database and Wordpress image

1. Open CMD with administrator permissions.
2. Change directory to the cloned repository folder.
3. Run this commands:
    docker create --name smallstore_mysql_data -v /var/lib/mysql mysql

    docker create --name smallstore_wordpress_data -v /var/www/html wordpress

## Run Docker Compose

1. Open CMD with administrator permissions.
2. Change directory to the cloned repository folder.
3. Run this command:
    docker-compose up -d
4. When asked to share folder click "YES"


## Restore Database

1. Open CMD with administrator permissions.
2. Change directory to the cloned repository folder.
3. Run this command:
    docker exec -i <NAME OF YOUR CURRENT FOLDER>_db_1 mysql -uroot -psomewordpress wordpress < smallstore.sql
    (PLEASE BE CAREFUL: DO NOT WRITE THE NAME OF YOUR CURRENT FOLDER INSIDE "<>" JUST IGNORE THOSE.)

## Open website

1. Navigate to: http://localhost:8000 on your browser and you should be able to see the website.
2. Wordpress credentials are:
    user: store-admin
    password: WPstore2019